import tcp, time

PUERTO = 1234
socket_tcp = tcp.TCPServer()
socket_tcp.listen(PUERTO)


def manejar_nuevo_cliente(cliente):
    ip_cliente = cliente.remoteIP()

    def enviar_respuesta(respuesta):
        cliente.send(respuesta)

    def recibir_mensaje(mensaje):
        print("Se recibieron datos de " + ip_cliente + ": " + mensaje)
        enviar_respuesta("Respuesta")

    cliente.onReceive(recibir_mensaje)


def principal():
    socket_tcp.onNewClient(manejar_nuevo_cliente)


if __name__ == "__main__":
    while True:
        principal()
