import tcp, time

HOST = "209.165.208.3"
PUERTO = 1234

socket_tcp = tcp.TCPClient()
socket_tcp.connect(HOST, PUERTO)


def recibir_mensaje(mensaje):
    print("Se recibieron datos de " + HOST + ": " + mensaje)


def principal():
    socket_tcp.onReceive(recibir_mensaje)

    for numero_solicitud in range(1, 11):
        time.sleep(1)
        datos = "Solicitud " + str(numero_solicitud)
        socket_tcp.send(datos)


if __name__ == "__main__":
    principal()
