!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
service password-encryption
!
hostname RouterISP
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017E545-
!
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode rapid-pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 209.165.208.1 255.255.255.224
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 9.9.9.1 255.255.255.252
 duplex auto
 speed auto
!
interface Serial0/0/0
 ip address 9.9.9.5 255.255.255.252
 clock rate 64000
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router rip
 version 2
 network 9.0.0.0
 network 209.165.208.0
 no auto-summary
!
ip classless
!
ip flow-export version 9
!
!
!
banner motd #Bienvenido a RouterISP#
!
!
!
!
!
line con 0
 password 7 0822455D0A16
 logging synchronous
 login
!
line aux 0
!
line vty 0 4
 password 7 0822455D0A16
 login
line vty 5 15
 login
!
!
!
end

